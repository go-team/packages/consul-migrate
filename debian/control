Source: consul-migrate
Section: devel
Priority: extra
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Tianon Gravi <tianon@debian.org>
Build-Depends: debhelper (>= 9),
               dh-golang,
               golang-github-hashicorp-raft-boltdb-dev,
               golang-github-hashicorp-raft-dev,
               golang-github-hashicorp-raft-mdb-dev,
               golang-go
Standards-Version: 3.9.6
Homepage: https://github.com/hashicorp/consul-migrate
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-go/packages/golang-github-hashicorp-consul-migrate.git
Vcs-Git: git://anonscm.debian.org/pkg-go/packages/golang-github-hashicorp-consul-migrate.git
XS-Go-Import-Path: github.com/hashicorp/consul-migrate

Package: consul-migrate
Architecture: any
Built-Using: ${misc:Built-Using}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: Consul server data migrator
 consul-migrate is a Go package and CLI utility to perform a very specific data
 migration for Consul servers nodes. Between Consul versions 0.5.0 and 0.5.1,
 the backend for storing Raft data was changed from LMDB to BoltDB. To support
 seamless upgrades, this library is embedded in Consul version 0.5.1 to perform
 the upgrade automatically.
 .
 This package contains the "consul-migrate" CLI utility necessary for upgrading
 from "< 0.5.1" to "0.6.0+".

Package: golang-github-hashicorp-consul-migrate-dev
Architecture: all
Depends: golang-github-hashicorp-raft-boltdb-dev,
         golang-github-hashicorp-raft-dev,
         golang-github-hashicorp-raft-mdb-dev,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Consul server data migrator (source)
 consul-migrate is a Go package and CLI utility to perform a very specific data
 migration for Consul servers nodes. Between Consul versions 0.5.0 and 0.5.1,
 the backend for storing Raft data was changed from LMDB to BoltDB. To support
 seamless upgrades, this library is embedded in Consul version 0.5.1 to perform
 the upgrade automatically.
 .
 This package contains the source.
